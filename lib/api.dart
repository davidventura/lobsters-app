// This is the class for the JSON returned from the homepage.
class LobsterData {
  final List<LobsterItem> items;

  LobsterData({required this.items});
  factory LobsterData.fromJson(List<dynamic> parsedJson) {
    List<LobsterItem> items = parsedJson.map((i) => LobsterItem.fromJson(i)).toList();
    return LobsterData(items: items);
  }
}

// This is the class for each item, used in various other places.
// TODO: Add more properties, this isn't all of them.
class LobsterItem {
  final String short_id;
  final String short_id_url;
  final String url;
  final DateTime created_at;
  final String title;
  final int score;

  LobsterItem(
      {required this.score,
      required this.short_id,
      required this.title,
      required this.created_at,
      required this.short_id_url,
      required this.url});
  factory LobsterItem.fromJson(Map<String, dynamic> parsedJson) {
    return new LobsterItem(
        created_at: DateTime.parse(parsedJson['created_at']),
        short_id: parsedJson['short_id'],
        short_id_url: parsedJson['short_id_url'],
        url: parsedJson['url'],
        title: parsedJson['title'],
        score: parsedJson['score']);
  }
}

// This is the parent object that contains the comments on a comments page.
class LobsterComments {
  final List<Comment> items;

  LobsterComments({required this.items});
  factory LobsterComments.fromJson(Map<String, dynamic> parsedJson) {
    var comments = parsedJson["comments"] as List;
	final converted = Comment.fromJson(comments.first);
    List<Comment> items = comments.map((i) => Comment.fromJson(i)).toList();
    return LobsterComments(items: items);
  }
}

// Each individual comment looks like this.
// TODO: Add more properties, this isn't all of them.
class Comment {
  final String short_id;
  final DateTime created_at;
  final String comment;
  final String comment_text;
  final User commenting_user;
  final int indent_level;
  final int score;

  Comment(
      {required this.score,
      required this.short_id,
      required this.created_at,
      required this.comment,
      required this.comment_text,
      required this.commenting_user,
      required this.indent_level});
  factory Comment.fromJson(Map<String, dynamic> parsedJson) {
	var ret = Comment(
      created_at: DateTime.parse(parsedJson['created_at']),
      short_id: parsedJson['short_id'],
      score: parsedJson['score'],
      comment: parsedJson['comment'],
      comment_text: parsedJson['comment_plain'],
      commenting_user: User.fromJson(parsedJson['commenting_user']),
      indent_level: parsedJson['depth'] + 1,
    );
    return ret;
  }
}

// Each individual user looks like this.
// TODO: Add more properties, this isn't all of them.
class User {
  final String username;
  final String avatar_url;
  final DateTime created_at;
  final bool is_admin;

  User({required this.username, required this.avatar_url, required this.created_at, required this.is_admin});
  factory User.fromJson(Map<String, dynamic> parsedJson) {
    return User(
      created_at: DateTime.parse(parsedJson['created_at']),
      username: parsedJson['username'],
      avatar_url: parsedJson['avatar_url'],
      is_admin: parsedJson['is_admin'],
    );
  }
}
