import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:lobsters_app/api.dart';
import 'package:lobsters_app/comments.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:http/http.dart' as http;

class HomeList extends StatefulWidget {
  final String endpoint;

  HomeList({required this.endpoint});

  @override
  HomeListState createState() {
    return new HomeListState();
  }
}

class HomeListState extends State<HomeList> {
  Future<LobsterData> data() async {
    var response = await http.get(Uri.parse(widget.endpoint));
    if (response.statusCode == 200) {
      // Everything is Good.
      return LobsterData.fromJson(json.decode(response.body));
    } else {
      // TODO: Fix this.
      throw Exception('Oops');
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<LobsterData>(
      future: data(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return ListView.builder(
            itemCount: snapshot.data!.items.length,
            itemBuilder: (context, index) {
              return new Material(
                child: InkWell(
                  child: Slidable(
                    child: new ListTile(
                      title: Text(snapshot.data!.items[index].title),
                      subtitle: Text(
                          snapshot.data!.items[index].created_at.toString()),
                      leading: new CircleAvatar(
                        child:
                            Text(snapshot.data!.items[index].score.toString()),
                      ),
                    ),
					startActionPane: ActionPane(
						// A motion is a widget used to control how the pane animates.
						extentRatio: 0.2,
						motion: const ScrollMotion(),
						children: [
						  SlidableAction(
							label: "Read Article",
							backgroundColor: Colors.blue,
							icon: Icons.link,
							onPressed: (_) => launch(snapshot.data!.items[index].url),
						  ),
						],
					),
                  ),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => CommentView(
                                item: snapshot.data!.items[index],
                              ),
                        ));
                  },
                ),
              );
            },
          );
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }
}
